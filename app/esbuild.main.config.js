import * as path from 'path'
import { copy } from 'esbuild-plugin-copy'

/** @var {Partial<import('esbuild').BuildOptions>} */
export default {
  platform: 'node',
  entryPoints: [
    path.resolve('src/main/main.js'),
    path.resolve('src/main/preload.js'),
  ],
  bundle: true,
  sourcemap: false,
  target: 'node18.15.0', // electron version target
  plugins: [
    copy({
      // this is equal to process.cwd(), which means we use cwd path as base path to resolve `to` path
      // if not specified, this plugin uses ESBuild.build outdir/outfile options as base path.
      resolveFrom: 'cwd',
      assets: [
        {
          from: ['./resources/**/*'],
          to: ['./dist/main/resources'],
        },
        {
          from: ['./src/renderer/index.html'],
          to: ['./dist/main/'],
        }
      ],
      watch: true,
    }),
  ],
}
