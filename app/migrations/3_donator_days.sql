CREATE TABLE donator_days (
  day Date,
  donator_id String,
  name String,
  city String,
  state String,
  zip_code String,
  employer String,
  amt AggregateFunction(sum, Float32),
  txns AggregateFunction(count, Float32)
) ENGINE = AggregatingMergeTree
ORDER BY (day, donator_id);
--
create materialized view donator_days_mv to donator_days as
select toStartOfDay(transaction_dt) as "day",
  concatWithSeparator(
    '-',
    fec.name,
    fec.city,
    fec.state,
    substring(fec.zip_code, 1, 5),
    fec.employer
  ) as donator_id,
  name,
  city,
  state,
  substring(fec.zip_code, 1, 5) as zip_code,
  employer,
  sumState(fec.transaction_amt) as amt,
  countState(fec.image_num) as txns
from fec -- inner join (
  --   SELECT concatWithSeparator(
  --       '-',
  --       name,
  --       city,
  --       state,
  --       substring(zip_code, 1, 5),
  --       employer
  --     ) AS donator_id,
  --     toStartOfDay(transaction_dt) as tx_day,
  --     COUNT(*) AS transaction_count
  --   FROM fec
  --   GROUP BY donator_id,
  --     tx_day
  --   HAVING transaction_count > 5
  -- ) s on s.donator_id = concatWithSeparator(
  --   '-',
  --   fec.name,
  --   fec.city,
  --   fec.state,
  --   substring(fec.zip_code, 1, 5),
  --   fec.employer
  -- )
group by donator_id,
  name,
  city,
  state,
  zip_code,
  employer,
  "day";