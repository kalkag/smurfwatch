CREATE TABLE smurfs (
  donator_id String,
  name String,
  city String,
  state String,
  zip_code String,
  employer String,
) ENGINE = ReplacingMergeTree PRIMARY KEY (donator_id);
--
create materialized view smurfs_mv to smurfs as
select d.donator_id,
  d.name,
  d.city,
  d.state,
  d.zip_code,
  d.employer
from donators d
  inner join (
    select distinct(donator_id)
    from (
        select day,
          donator_id,
          countMerge(txns) as txns,
          sumMerge(amt) as amt
        from donator_days
        group by day,
          donator_id
        having txns > 10
      )
  ) s on s.donator_id = d.donator_id