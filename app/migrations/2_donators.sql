create table if not exists donators (
  donator_id String,
  name String,
  city String,
  state String,
  zip_code String,
  employer String
) ENGINE = ReplacingMergeTree PRIMARY KEY (donator_id);
create materialized view donators_mv to donators as
select concatWithSeparator(
    '-',
    name,
    city,
    state,
    substring(zip_code, 1, 5),
    employer
  ) as donator_id,
  name,
  city,
  state,
  substring(zip_code, 1, 5) as zip_code,
  employer
from fec
group by donator_id,
  name,
  city,
  state,
  zip_code,
  employer;