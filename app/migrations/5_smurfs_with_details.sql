CREATE TABLE smurfs_with_details (
  donator_id String,
  name String,
  city String,
  state String,
  zip_code String,
  employer String,
  amt Float32,
  txns Float32
) ENGINE = AggregatingMergeTree()
ORDER BY (state, donator_id);
--
create materialized view smurfs_with_details_mv to smurfs_with_details as
select d.donator_id,
  d.name,
  d.city,
  d.state,
  d.zip_code,
  d.employer,
  sumMerge(dd.amt) as amt,
  countMerge(dd.txns) as txns
from donators d
  inner join donator_days dd on dd.donator_id = d.donator_id
group by d.donator_id,
  d.name,
  d.city,
  d.state,
  d.zip_code,
  d.employer;