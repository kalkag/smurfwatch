import Store from 'electron-store';

// declare global {
//   var bus: Bus
// }

if (!global.bus) {
  global.store = new Store();

  Store.initRenderer();
}

export default global.store;