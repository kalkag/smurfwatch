import fs from 'node:fs';
import crypto from 'node:crypto';
import path from 'node:path';
import { spawn } from 'node:child_process';
import store from './lib/store'

export function getClickhousePath() {
  let clickhousePath = store.get('clickhousePath')

  if (!clickhousePath) {
    // Try to find clickhouse in the path
    const paths = process.env.PATH.split(path.delimiter);
    for (const p of paths) {
      const ch = path.join(p, 'clickhouse');
      if (fs.existsSync(ch)) {
        clickhousePath = ch;
        store.set('clickhousePath', ch);
        break;
      }
    }
  }

  if (!clickhousePath) throw new Error('Path to clickhouse binary not set');
  if (!fs.existsSync(clickhousePath)) throw new Error('Path to clickhouse binary does not exist');

  return clickhousePath;
}

async function readCachedFile(cachedFile) {
  const clickhousePath = getClickhousePath();

  return new Promise((resolve, reject) => {
    const chunks = [];
    const stm = spawn(clickhousePath, [
      'local',
      '--input_format_allow_errors_ratio=1',
      `-q SELECT * FROM file('${cachedFile}', Parquet) FORMAT JSON`,
    ]);

    stm.stdout.on('data', (chunk) => {
      chunks.push(chunk)
    });

    stm.on('close', () => {
      const result = Buffer.concat(chunks).toString('utf8')

      if (!result) return resolve([])

      resolve(JSON.parse(result).data)
    });

    stm.stderr.on('data', (err) => {
      console.error('ClickHouse query error', err.toString());
      reject(err.toString())
    });
  });
}

export async function convertCsvToParquet(csvFile) {
  const clickhousePath = getClickhousePath();
  const dataPath = store.get('dataPath')
  const cacheDir = path.join(dataPath, '.cache');
  if (!fs.existsSync(cacheDir)) fs.mkdirSync(cacheDir, { recursive: true })
  
  const cvsDirectory = path.dirname(csvFile);
  const parquetFileName = path.basename(csvFile, '.txt') + '.parquet';
  const parquetFilePath = path.join(cvsDirectory, parquetFileName);

  console.log({ parquetFileName, parquetFilePath})
  
  const q = `
    SELECT
      *
    FROM file('${csvFile}', CSV, 'cmte_id String, amndt_ind String, rpt_tp String, transaction_pgi String, image_num String, transaction_tp String, entity_tp String, name String, city String, state String, zip_code String, employer String, occupation String, transaction_dt String, transaction_amt Float32, other_id String, tran_id String, file_num String, memo_cd String, memo_text String, sub_id String')
    INTO OUTFILE '${parquetFilePath}' FORMAT parquet
  `;

  // console.log({ parquet_q: q });
  
  return new Promise((resolve, reject) => {
    const stm = spawn(clickhousePath, [
      'local', `--format_csv_delimiter=|`, '--format_csv_allow_single_quotes=false',
      '--input_format_allow_errors_ratio=1',
      '--format_csv_allow_double_quotes=false', `-q ${q}`,
    ]);

    stm.on('close', async () => {
      resolve(parquetFilePath);
    });

    stm.stderr.on('data', (err) => {
      console.error('ClickHouse CSV->Parquet query error', err.toString());
      reject(err.toString())
    });
  });
}

async function query(q, opts = {}) {
  // README: https://clickhouse.com/docs/en/operations/utilities/clickhouse-local

  const clickhousePath = getClickhousePath();
  const dataPath = store.get('dataPath')
  const cacheDir = path.join(dataPath, '.cache');
  if (!fs.existsSync(cacheDir)) fs.mkdirSync(cacheDir, { recursive: true })
  console.log({ cacheDir })

  if (!dataPath) throw new Error('Data path not set');
  const filesPath = path.join(dataPath, 'extracted');

  let globPath = `${filesPath}/**`
  if (opts?.dirs?.length > 1) {
    globPath = `${filesPath}/{${opts.dirs.join(',')}}/**`
  } else if (opts?.dirs?.length === 1) {
    globPath = `${filesPath}/${opts.dirs[0]}/**`
  }

  q = q.replace(/FROM\s+\w+/, `FROM file('${globPath}', Parquet)`);

  const hash = crypto.createHash('sha1').update(q).digest('hex');
  console.log({ q })

  const cachedResultsFile = path.join(cacheDir, `${hash}.parquet`);
  if (fs.existsSync(cachedResultsFile)) {
    console.log('Using cached results from', cachedResultsFile);
    return readCachedFile(cachedResultsFile);
  }

  return new Promise((resolve, reject) => {
    // const chunks = [];
    const stm = spawn(clickhousePath, [
      'local', `--format_csv_delimiter=|`, '--format_csv_allow_single_quotes=false',
      '--input_format_allow_errors_ratio=1',
      '--format_csv_allow_double_quotes=false', `-q ${q} FORMAT Parquet`,
    ]);

    const writePipe = fs.createWriteStream(cachedResultsFile, 'utf-8');

    stm.stdout.pipe(writePipe);

    stm.stdout.on('data', (chunk) => {
      // chunks.push(chunk)
    });

    stm.on('close', async () => {
      // const result = Buffer.concat(chunks).toString('utf8')

      // if (!result) return resolve([])

      // resolve(JSON.parse(result).data)

      resolve(await readCachedFile(cachedResultsFile));
    });

    stm.stderr.on('data', (err) => {
      console.error('ClickHouse query error', err.toString());
      reject(err.toString())
    });
  });
}

const clickhouse = {
  convertCsvToParquet,
  query,
}

export default clickhouse;