import { contextBridge, ipcRenderer } from 'electron'
import { electronAPI } from '@electron-toolkit/preload'

if (process.contextIsolated) {
  try {
    contextBridge.exposeInMainWorld('electron', electronAPI)
  } catch (error) {
    console.error(error)
  }
} else {
  window.electron = electronAPI
}

// const clickhouse = new ClickHouse({
//   url: 'http://localhost', // Replace with your ClickHouse server URL
//   port: 8123, // Replace with your ClickHouse server port
//   debug: false,
//   basicAuth: {
//     username: 'default',
//     password: 'default',
//   },
// });

contextBridge.exposeInMainWorld('clickhouse', {
  query: (...args) => ipcRenderer.invoke('clickhouse.query', ...args),
});

contextBridge.exposeInMainWorld('versions', {
  node: () => process.versions.node,
  chrome: () => process.versions.chrome,
  electron: () => process.versions.electron
});
