class PEmit extends Promise {
  onEmit(callback) {
		if (typeof callback !== 'function') {
			throw new TypeError(`Expected a \`Function\`, got \`${typeof callback}\``);
		}

    this._listeners.add(callback);
    
		return this;
  }
  
  constructor(executor) {
    const emit = event => {
			(async () => {
				// We wait for the next microtask tick so `super` is called before we use `this`
				await Promise.resolve();

				for (const listener of this._listeners) {
					listener(event);
				}
			})();
		};

    super((resolve, reject) => {
			executor(
				value => {
					resolve(value);
				},
				reject,
				event => {
					emit(event)
				},
			);
    });
    
    this._listeners = new Set();
		this._emit = emit;
  }
}

export default function pEmit(input) {
	return new PEmit(async (resolve, reject, emit) => {
		try {
			resolve(await input(emit));
		} catch (error) {
			reject(error);
		}
	});
}