import fs, { mkdirSync } from 'fs';
import path from 'path';
import StreamZip from 'node-stream-zip';
import pMap from 'p-map';
import { memoize } from 'lodash';
import pEmit from './pEmit';
import store from './lib/store';
import { convertCsvToParquet } from './clickhouse';

async function getGoodZipEntries(zipFile) {
  const zip = new StreamZip.async({ file: zipFile });

  const entries = Object.values(await zip.entries());

  const hasByDateFiles = entries.some(x => /\_\d{4}\_/.test(x.name));

  const goodEntries = entries.filter(x => {
    return !x.name.includes('invalid')
      && (((hasByDateFiles && x.name !== 'itcont.txt')) || !hasByDateFiles);
  });

  return goodEntries;
}

// TODO: is there really anything here to check? If the file exists and is extracted, that's it...
export const checkZipFile = memoize(async (zipFile, clear = false) => {
  const dataDirectory = store.get('dataPath');
  const extractedDataDirectory = path.join(dataDirectory, 'extracted');
  
  if (clear) checkZipFile.cache.clear();

  const zipExtractDataDirecty = path.join(extractedDataDirectory, path.basename(zipFile, '.zip'));
  if (!fs.existsSync(zipExtractDataDirecty)) fs.mkdirSync(zipExtractDataDirecty, { recursive: true });

  try {
    const entries = await getGoodZipEntries(zipFile);

    const missing = entries.filter(x => {
      return !fs.existsSync(path.join(zipExtractDataDirecty, path.basename(x.name)))
        && !fs.existsSync(path.join(zipExtractDataDirecty, path.basename(x.name, '.txt') + '.parquet'));
    })

    console.log({ zipFile, missing: missing.map(x => x.name) })

    return {
      complete: missing.length === 0,
    }
  } catch (err) {
    console.error(`Error checking zip file: ${zipFile}`, err.message);

    return {
      complete: false,
    }
  }
});

const processZipFile = zipfile => pEmit(async emit => {
  console.log(`... Processing zip file ${zipfile} ...`);
  const dataDirectory = store.get('dataPath');
  const extractedDataDirectory = path.join(dataDirectory, 'extracted');

  if (!fs.existsSync(zipfile)) throw new Error(`File ${zipfile} does not exist`);

  const basename = path.basename(zipfile, '.zip');
  const extractPath = path.join(extractedDataDirectory, basename);

  // See if we've already extracts this file
  if (!fs.existsSync(extractPath)) mkdirSync(extractPath, { recursive: true });

  console.log({ extractPath })
  const unzippedFiles = fs.readdirSync(extractPath, { recursive: true }).filter(x => x.endsWith('.txt'));
  console.log({ unzippedFiles });

  // yauzl.open("path/to/file.zip", { lazyEntries: true }, function (err, zipfile) {
  //   if (err) throw err;
  //   zipfile.readEntry();
  //   zipfile.on("entry", function(entry) {
  //     if (/\/$/.test(entry.fileName)) {
  //       // Directory file names end with '/'
  //       zipfile.readEntry();
  //     } else {
  //       // file entry
  //       zipfile.openReadStream(entry, function(err, readStream) {
  //         if (err) throw err;
  //         readStream.on("end", function() {
  //           zipfile.readEntry();
  //         });

  //         readStream.pipe(fs.createWriteStream(entry.fileName));
  //       });
  //     }
  //   });
  // })
  
  const zip = new StreamZip.async({ file: zipfile });
  zip.on('error', (err) => {
    console.error('Error processing zip file', err);
    throw err;
  });

  // get year from zipfile name
  const zipBasename = path.basename(zipfile, '.zip');
  
  const entries = await getGoodZipEntries(zipfile);

  // Find entries in the zip that have not been extracted
  const missingEntries = entries.filter(x => !unzippedFiles.includes(path.basename(x.name)));
  console.log({ missingEntries: missingEntries.map(x => x.name)});
  
  const zipExtractDataDirecty = path.join(extractedDataDirectory, zipBasename);
  if (!fs.existsSync(zipExtractDataDirecty)) fs.mkdirSync(zipExtractDataDirecty, { recursive: true });

  await pMap(missingEntries, async (missingEntry, i) => {
    await new Promise(async (resolve, reject) => {
      emit({ eventType: 'zip_file', filename: zipfile, state: 'processing', progress: (i - 1) / missingEntries.length * 100, currentFile: path.basename(missingEntry.name) });

      console.log(`Extracting missing entry ${missingEntry.name} to ${zipExtractDataDirecty}...`)
      const extractedPath = path.join(zipExtractDataDirecty, path.basename(missingEntry.name));

      if (!fs.existsSync(extractedPath)) {
        console.info('Extracting missing entry', missingEntry.name);
        emit({ eventType: 'zip_file', filename: zipfile, state: 'processing', progress: (i + 1) / missingEntries.length * 100, currentFile: path.basename(missingEntry.name) });
      
        const exStream = fs.createWriteStream(extractedPath, { flags: 'w' });
        exStream.on('error', (err) => {
          console.error(`Error extracting ${missingEntry.name}`, err);
        });

        const stream = await zip.stream(missingEntry.name);
        stream.pipe(exStream);
        stream.on('error', (err) => {
          console.error(`Error extracting ${missingEntry.name}`, err);
          reject(err);
        })
        stream.on('end', async () => {
          console.info(`Finished extracting ${missingEntry.name}`);
          console.info(`Converting ${missingEntry.name} to parquet...`);
          await convertCsvToParquet(extractedPath);
          console.info(`Unlinking ${extractedPath}...`);
          await fs.unlinkSync(extractedPath);

          emit({ eventType: 'zip_file', filename: zipfile, state: 'processing', progress: (i + 1) / missingEntries.length * 100, currentFile: path.basename(missingEntry.name) });
          resolve();
        })
      }
    });
  }, { concurrency: 1 });
  
  await zip.close();
  await checkZipFile(zipfile, true);

  emit({ eventType: 'zip_file', filename: zipfile, state: 'processed', processed: true, progress: 100 });
});

export default processZipFile;