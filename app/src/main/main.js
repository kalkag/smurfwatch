import fs from 'node:fs'
import path from 'node:path'
import util from 'node:util'
import { exec as _exec } from 'node:child_process'
import { app, BrowserWindow, Tray, dialog, ipcMain, nativeImage, shell } from 'electron'
import log from 'electron-log';
import { is } from 'electron-util'
import { format } from 'url'
import processZipFile, { checkZipFile } from './processFile.js';
import clickhouse, { getClickhousePath } from './clickhouse';
import store from './lib/store';

const exec = util.promisify(_exec);

const domain = "https://www.fec.gov";
// const dataDirectory = path.join(app.getPath('userData'), 'smurfwatch_data');

/** @type {BrowserWindow | null} */
let win = null

process.on('unhandledRejection', (reason, promise) => {
  console.error('Unhandled Rejection at:', promise, 'reason:', reason)
});

process.on('uncaughtException', (err) => {
  console.error('Uncaught Exception:', err);
})

async function createWindow() {
  const iconPath = path.join(app.getAppPath(), 'resources/logo.png')
  const trayIcon = nativeImage.createFromPath(iconPath)

  const tray = new Tray(trayIcon)
  tray.setTitle('SmuftWatch')
  tray.setToolTip('SmuftWatch')

  app.dock.setIcon(trayIcon)
  
  win = new BrowserWindow({
    titleBarStyle: 'hidden',
    trafficLightPosition: { x: 10, y: 12 },
    width: 1200,
    height: 820,
    minHeight: 600,
    minWidth: 650,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      preload: path.join(__dirname, 'preload.js'),
      webSecurity: false,
    },
    show: false,
  })
  const ses = win.webContents.session

  const isDev = is.development

  console.log({ isDev })

  if (isDev) {
    // this is the default port electron-esbuild is using
    win.loadURL('http://localhost:9080')
  } else {
    win.loadURL(
      format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true,
      }),
    )
  }

  win.on('closed', () => {
    win = null
  })

  win.webContents.on('devtools-opened', () => {
    win.focus()
  })

  win.on('ready-to-show', async () => {
    win.show()
    win.focus()

    if (isDev) {
      win.webContents.openDevTools({ mode: 'bottom' })
    }

    await init()
  })

  async function processFile(filename) {
    const dataDirectory = store.get('dataPath');

    const p = processZipFile(path.join(dataDirectory, filename));

    p.onEmit(event => {
      win.webContents.send('processing-info', {
        ...event,
        filename,
      });
    });

    await p;
  }

  ses.on('will-download', (event, item, webContents) => {
    console.log('! === Downloading', item.getFilename());
    const dataDirectory = store.get('dataPath');
    
    if (item.getState() === 'interrupted') {
      item.resume();
    } else {
      // Set the save path, making Electron not to prompt a save dialog.
      const savePath = path.join(dataDirectory, item.getFilename())
        
      item.setSavePath(savePath)
    }

    win.webContents.send('download-info', {
      state: 'started',
      filename: item.getFilename(),
      totalBytes: item.getTotalBytes(),
      receivedBytes: item.getReceivedBytes(),
    });

    item.on('updated', (event, state) => {
      win.webContents.send('download-info', {
        state,
        filename: item.getFilename(),
        totalBytes: item.getTotalBytes(),
        receivedBytes: item.getReceivedBytes(),
      });
      
      if (state === 'interrupted') {
        console.log('Download is interrupted but can be resumed');
      } else if (state === 'progressing') {
        if (item.isPaused()) {
          console.log('Download is paused')
        } else {
          console.log(`Received bytes: ${item.getReceivedBytes()}`)
        }
      }
    })

    item.once('done', (event, state) => {
      win.webContents.send('download-info', {
        state,
        filename: item.getFilename(),
        totalBytes: item.getTotalBytes(),
        receivedBytes: item.getReceivedBytes(),
      });

      if (state === 'completed') {
        console.log('Download successfully')

        processFile(item.getFilename())
      } else {
        console.log(`Download failed: ${state}`)
      }
    })
  })

  ipcMain.handle('init.readyChecks', async (_) => {
    const ret = {
      clickhouseOk: true,
      dataDirectoryOk: true,
    }

    // Do we have a path to clickhouse?
    let clickhousePath
    try {
      clickhousePath = getClickhousePath()
    } catch (err) {
      ret.clickhouseOk = { message: err.message };
    }

    console.log('clickhousePath', clickhousePath)

    if (clickhousePath) {
      // Try to run the clickhouse binary to see if it's working
      let stdout, stderr;
      try {
        ({ stdout, stderr } = await exec(`${clickhousePath} --version`))

        if (!stdout.includes('ClickHouse local version')) {
          ret.clickhouseOk = { message: `File specified for clickhouse doesn't appear to be correct.  Expected output with --version flag to say "ClickHouse local version". Actual output: "${stdout}"` };
        }
      } catch (err) {
        ret.clickhouseOk = { message: err.message };
      }
    } else {
      ret.clickhouseOk = { message: 'Path to clickhouse binary not set' };
    }

    // Do we have a path to the data directory?
    const dataPath = store.get('dataPath');
    if (!dataPath) ret.dataDirectoryOk = { message: 'Data directory not set' };
    else if (!fs.existsSync(dataPath)) ret.dataDirectoryOk = { message: `Data directory "${dataPath}" does not exist` };
    
    return ret;
  })

  ipcMain.handle('dialog:openFile', async (event, ...args) => {
    const { canceled, filePaths } = await dialog.showOpenDialog(...args)

    if (!canceled) {
      return filePaths[0]
    }
  });

  ipcMain.handle('getStoreValue', (event, key) => {
    console.log('== getting store value', key, store.get(key));
	  return store.get(key);
  });

  ipcMain.handle('setStoreValue', (event, key, value) => {
    console.log('== Setting store value', key, value);

	  return store.set(key, value);
  });

  ipcMain.on('download', async (_, file) => {
    // TODO: throw if we can't download to the data directory
    // if (!fs.existsSync(dataPath)) fs.mkdirSync(datPath);

    ses.downloadURL(`${domain}${file.href}`);
  });

  ipcMain.on('process', async (_, file) => {
    await processFile(file.filename);

    // const dataDirectory = store.get('dataPath');

    // const p = processZipFile(path.join(dataDirectory, file.filename));

    // p.onEmit(event => {
    //   win.webContents.send('processing-info', {
    //     ...event,
    //     filename: file.filename
    //   });
    // });

    // await p;
  });

  ipcMain.handle('list-files', async (_, args) => {
    const dataDirectory = store.get('dataPath');

    const files = fs.readdirSync(dataDirectory, { withFileTypes: true })
      .filter(dirent => dirent.isFile() && dirent.name.endsWith('.zip'))
      .map(dirent => dirent.name);
    
    console.log({ files })

    return Promise.all(files.map(async (file) => {
      const filePath = path.join(dataDirectory, file);

      return {
        filename: file,
        receivedBytes: fs.statSync(filePath, file).size || 0,
        processed: (await checkZipFile(filePath, true)).complete,
      };
    }));
  });

  ipcMain.handle('extracted-dirs', async (_, args) => {
    const dataDirectory = store.get('dataPath');

    if (!fs.existsSync(path.join(dataDirectory, 'extracted'))) {
      return [];
    }

    const files = fs.readdirSync(path.join(dataDirectory, 'extracted'), { withFileTypes: true })
      .filter(dirent => dirent.isDirectory())
      .map(dirent => dirent.name);

    return files;
  });

  ipcMain.handle('clickhouse.query', async (_, query, ...args) => {
    // console.log('... Querying ClickHouse ...', args);
    // const clickhouse = getClient();

    // // time the query
    // const start = Date.now();
    // const res = await clickhouse.query(...args);
    // const end = Date.now();
    // console.log(`... Query took ${end - start}ms ...`);

    // const json = await res.json();
    
    try {
      const result = await clickhouse.query(query, ...args);

      return result;
    } catch (err) {
      throw err;
    }
  });

  ipcMain.on('openLink', async (_, url) => {
    shell.openExternal(url);
  })

  ipcMain.on('print', () => {
    const dataPath = store.get('dataPath');
    const filepath1 = path.join(dataPath, 'print1.pdf'); 

    const options = {
      marginsType: 0,
      pageSize: 'A4',
      printBackground: true,
      printSelectionOnly: false,
      landscape: true
    }

    win.webContents.printToPDF(options).then(data => {
      fs.writeFile(filepath1, data, function (err) {
        if (err) {
            console.log(err);
        } else {
          console.log(`PDF Generated Successfully: ${filepath1}`);
          
          shell.openPath(filepath1)
        }
      });
    }).catch(error => {
      log(error);
    });
  })
}

app.on('ready', () => {
  console.log('... App is ready ...')
  createWindow()
})

app.on('window-all-closed', () => {
  if (!is.macos) {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null && app.isReady()) {
    createWindow()
  }
})

async function init() {
  await Promise.all([
    checkFiles(),
  ])
}

async function checkFiles() {
  const dataDirectory = store.get('dataPath');

  const files = fs.readdirSync(dataDirectory).filter(x => x.endsWith('.zip'));
  console.log(`... Already downloaded ${files.length} file${files.length !== 1 ? 's' : ''} ...`);

  for (const file of files) {
    const stat = fs.statSync(path.join(dataDirectory, file))
    const size = stat.size;

    const year = file.match(/(?<year>\d\d)/).groups.year;
    const prefix = year >= 80 ? '19' : '20';
    const remoteUrl = `${domain}/files/bulk-downloads/${prefix}${year}/${file}`
    const ret = (await fetch(remoteUrl, { method: 'HEAD' }));
    console.log({ headers: ret.headers.entries() });
    const remoteSize = ret.headers.get('content-length');

    console.log({ file, size, remoteSize, percent: (size / parseInt(remoteSize, 10)) * 100 });

    if (size < remoteSize) {
      console.log(`... ${file} is incomplete, downloading the rest ...`);
      const ses = win.webContents.session
      // ses.downloadURL(remoteUrl);
      ses.createInterruptedDownload({
        path: path.join(dataDirectory, file),
        urlChain: [ret.url],
        length: parseInt(remoteSize, 10),
        offset: size,
        lastModified: stat.mtime,
        eTag: ret.headers.get('etag'),
      });
    }
  }
}