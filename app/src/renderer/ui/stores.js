import { writable } from 'svelte/store';

export const selectedTab = writable(null);
export const selectedDonator = writable(null);

window.electron.ipcRenderer.invoke("getStoreValue", "selectedTab")
  .then(selectedTabValue => {
    console.log('getStoreValue selectedTab', selectedTabValue)
    if (selectedTabValue) selectedTab.set(selectedTabValue)
  });

window.electron.ipcRenderer.invoke("getStoreValue", "selectedDonator")
  .then(selectedDonatorValue => {
    console.log('getStoreValue selectedDonator', selectedDonatorValue)
    if (selectedDonatorValue) selectedDonator.set(selectedDonatorValue)
  });

console.log('=== stores.js loaded')
selectedTab.subscribe((value) => {
  console.log('setStoreValue selectedTab', value)
  window.electron.ipcRenderer.invoke("setStoreValue", "selectedTab", value);
});

selectedDonator.subscribe((value) => {
  console.log('setStoreValue selectedDonator', value)
  window.electron.ipcRenderer.invoke("setStoreValue", "selectedDonator", value);
});