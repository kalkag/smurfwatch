import { LocalStorageLRU } from '@cocalc/local-storage-lru';

const storage = new LocalStorageLRU();

export default storage;