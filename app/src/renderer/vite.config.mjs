import { svelte } from '@sveltejs/vite-plugin-svelte'
import { defineConfig } from 'vite'
import sveltePreprocess from 'svelte-preprocess'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte({ preprocess: sveltePreprocess() })],
  build: {
    target: 'chrome114', // electron version target
  },
  server: {
    port: 9080
  },
})


// import { defineConfig } from 'vite';
// import { svelte } from '@sveltejs/vite-plugin-svelte';

// export default defineConfig({
//   plugins: [svelte()],
//   build: {
//     target: 'chrome114', // electron version target
//   },
//   server: {
//     port: 9080
//   },
// });
