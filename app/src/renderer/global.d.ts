/// <reference types="svelte" />
/// <reference types="vite/client" />
import { ElectronAPI } from '@electron-toolkit/preload'
import { ClickHouseClient } from '@clickhouse/client'

declare global {
  interface Window {

    clickhouse: ClickHouseClient
    electron: ElectronAPI
  }
}