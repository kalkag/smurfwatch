/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{svelte,js,ts}'],
  purge: ["./index.html", './src/**/*.{svelte,js,ts}'],
  
  plugins: [require('daisyui')],

  darkMode: false,
};
