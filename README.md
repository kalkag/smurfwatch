# SmurfWatch

SmurfWatch is a research tool for finding "smurf" donators, or persons whose identities have been used to make
fraudulent campaign contributions.

The average American makes 1.4 political donations per year. "Smurfs" make thousands of donations per year, often
several times per day, in amounts that can total several hundred thousand dollars. Smurfs are also often retired persons,
and have no idea that their identities are being used this way.

# How Does the App Work?

SmurfWatch lets you download bulk data from the FEC's [campaign reporting website](https://www.fec.gov/data/browse-data/?tab=bulk-data)
and query against it to find and generate reports on these records.

For analysis purposes, SmurfWatch defines a "smurf" as any person who has donated 10 or more times in a single day. This is a list of
smurf donors in the state of Georgia for the years 2021-2022. Note that while all of this data is publicly available, we are blurring the names here.

![Example Donator List](example_donator_list.png)

This is an example smurf donor, who (according to the FEC) has contributed $188K from 2021 to 2024, mostly through WinRed. On Nov 4th, 2022, just a few days before election day, they made 355 donations.



![Example report](example_donator_report.png)

The reports can be generated as PDFs for printing and sharing.

# Get Started

1. Download a release for your platform from the "Releases" page on this site, and install the app.
2. The app will guide you through a couple onboarding steps to install an open source data analysis tool called ["Clickhouse"](https://clickhouse.com/), and to choose a place to store data downloaded from the FEC stie.
3. Choose a state and start searching.


# FAQ

* **Q:** Why is this a tool I have to download and install, not an online app?
  
  **A:** Hosting a tool that allows many people to simultaneously query across tens or hundreds of gigabytes of data costs quite a bit of
  money. And hosting such a tool that can be used for political means can make it a target of attacks, such as Denial of Service or doxing.  Keeping it Open Source and runnable by anyone allows any party to replicate and verify results.


* **Q:** Where does this data come from?
  
  **A:** The FEC (Federal Election Commission) bulk data on individual campaign contributions primarily comes from the filings submitted by political committees, candidates, and other organizations to the FEC. These filings include detailed information about contributions received by candidates, political action committees (PACs), party committees, and other entities involved in federal elections.

  The FEC requires these entities to regularly file reports disclosing information such as the amount of each contribution, the identity of the contributor (including their name, address, and occupation), and other relevant details. This information is then compiled and made available to the public in various formats.

* **Q:** The amounts on the "Donator" page are different than the "Donators" list.

  **A:** For performance purposes, the "Donators" list only searches for days where the number of transactions is 10 or greater. This means that days that counts of transactions less than 10 won't be included, but they will show up on the full "Donator" report, so the "Donators" page will show a "minimum" dollar amount and number of transactions.
