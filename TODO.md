# TODO

# Bugs / Tasks
* We're hitting localStorage limits. We probably need to shift to doing caching in lowdb
  * We could use single-file caches based on keys. Basically cache every query in a single file. We could do a readable
    name for the file, or hash it.
  * Could we change the output mechanism from clickhouse local to be TSV and cache the results in a TSV, then read that
    into JSON? Would that be efficient?
* Failure to search donors when the extracted directory is missing.
* When you complete onboarding after you haven't onboarded
  * It should redirect you to the data page if you have no data
  * It should redirect you to the front page if you have data
  * It should NOT have no tab selected by default
* Need to handle removing the zip file once it's extracted, which means we won't be able to check and see if we have all
  * the extracted files
* Fix charts to not show time on date hover
* Files not showing up as "Processed" after downloaded and imported (try the 161MB one from 2014)
* DRY up the progress emitting in `processFile.js`
* clickhouse commands keep running (zombies). We need to identify and kill these.
  * This might only be a problem in local development though.. (w/ app restarts)


# Features
* After downloading and extracting a .zip, permanently cache the list of files in it and then remove the .zip
  (to reduce disk usage) so we can tell if we have ALL the extracted files. If we use .parquet, we need to account for =
  the extension change.
* Don't destroy the tabs if we move away; just show/hide them. Do visibility toggles
* Support for canceling queries if user navigates away
  * Track spawned processes in a list
  * Cancelable promises?  Maybe Sindre has something=
* Allow queries to keep running if you navigate away (handled by former?)
* Toast up errors
* Show download speed
* Allow removing extracted files (i.e. confirmable delete button)
* Disk space usage visualization on config page
* Add some kind of checks in the app for whether clickhouse is running and accessible. Add a button to restart the instance if it crashes
* Allow grouping donators. Some "distinct" ones seem to be the same but one is missing the employer.
* Onboarding: need to be able to download/install clickhouse locally.
* Filtering by year (should allow us to choose what files we target and make it faster)
* Highlight election days (or other important dates) in the axis, if possible.


# SQL

Get all smurfs

```sql
./clickhouse local --output-format=json --format_csv_delimiter='|' --format_csv_allow_single_quotes=false --format_csv_allow_double_quotes=false -q " \
SELECT \
name, city, state, zip_code, employer, toStartOfDay(parseDateTimeOrNull(transaction_dt, '%m%d%Y')) as "day", \
count(tran_id) as txns, sum(transaction_amt) as amt \
FROM file('./by_date/**', CSV, 'cmte_id String, amndt_ind String, rpt_tp String, transaction_pgi String, image_num String, transaction_tp String, entity_tp String, name String, city String, state String, zip_code String, employer String, occupation String, transaction_dt String, transaction_amt Float32, other_id String, tran_id String, file_num String, memo_cd String, memo_text String, sub_id String') \
GROUP BY name, city, state, zip_code, employer, "day" \
HAVING COUNT(tran_id) >= 10"
```

Get smurfs for a state

```sql
./clickhouse local --output-format=json --format_csv_delimiter='|' --format_csv_allow_single_quotes=false --format_csv_allow_double_quotes=false -q " \
SELECT \
distinct on(name, city, state, substring(zip_code, 1, 5), employer) name, city, state, zip_code, employer, toStartOfDay(parseDateTimeOrNull(transaction_dt, '%m%d%Y')) as "day", \
count(tran_id) as txns, sum(transaction_amt) as amt \
FROM file('./by_date/**', CSV, 'cmte_id String, amndt_ind String, rpt_tp String, transaction_pgi String, image_num String, transaction_tp String, entity_tp String, name String, city String, state String, zip_code String, employer String, occupation String, transaction_dt String, transaction_amt Float32, other_id String, tran_id String, file_num String, memo_cd String, memo_text String, sub_id String') \
WHERE state = 'NY'
GROUP BY name, city, state, zip_code, employer, "day" \
HAVING COUNT(tran_id) >= 10"
```

Get all transactions for a particular smurf

```sql
./clickhouse local --output-format=json --format_csv_delimiter='|' --format_csv_allow_single_quotes=false --format_csv_allow_double_quotes=false -q " \
SELECT \
name, city, state, substring(zip_code, 1, 5) as zip_code, toStartOfDay(parseDateTimeOrNull(transaction_dt, '%m%d%Y')) as "day", \
sum(transaction_amt) as amt, count(tran_id) as txns \
FROM file('./by_date/**', CSV, 'cmte_id String, amndt_ind String, rpt_tp String, transaction_pgi String, image_num String, transaction_tp String, entity_tp String, name String, city String, state String, zip_code String, employer String, occupation String, transaction_dt String, transaction_amt Float32, other_id String, tran_id String, file_num String, memo_cd String, memo_text String, sub_id String') \
WHERE name = 'VANDERPOOL, BARBARA' GROUP BY name, city, state, zip_code, employer, day"
```

# Parquet

```sql
/Users/brianh/repos/test/smurfwatch/app/clickhouse local --output-format=json --format_csv_delimiter='|' --format_csv_allow_single_quotes=false --format_csv_allow_double_quotes=false -q " \
SELECT \
name, city, state, zip_code, employer, toStartOfDay(parseDateTimeOrNull(transaction_dt, '%m%d%Y')) as "day", \
count(tran_id) as txns, sum(transaction_amt) as amt \
FROM file('./*.txt', CSV, 'cmte_id String, amndt_ind String, rpt_tp String, transaction_pgi String, image_num String, transaction_tp String, entity_tp String, name String, city String, state String, zip_code String, employer String, occupation String, transaction_dt String, transaction_amt Float32, other_id String, tran_id String, file_num String, memo_cd String, memo_text String, sub_id String') \
GROUP BY name, city, state, zip_code, employer, "day" \
HAVING COUNT(tran_id) >= 10"
```

```sql
/Users/brianh/repos/test/smurfwatch/app/clickhouse local --output-format=json -q " \
SELECT \
name, city, state, zip_code, employer, toStartOfDay(parseDateTimeOrNull(transaction_dt, '%m%d%Y')) as "day", \
count(tran_id) as txns, sum(transaction_amt) as amt \
FROM file('./*.parquet', Parquet) \
GROUP BY name, city, state, zip_code, employer, "day" \
HAVING COUNT(tran_id) >= 10"
```

Exapmple to convert .txt csv to parquet

```sql
./clickhouse --format_csv_delimiter='|' --format_csv_allow_single_quotes=false -q " \
SELECT \
  * \
FROM file('./extracted/indiv24/itcont_2024_20000401_20230330.txt', CSV, \
'cmte_id String, amndt_ind String, rpt_tp String, transaction_pgi String, image_num String, transaction_tp String, entity_tp String, name String, city String, state String, zip_code String, employer String, occupation String, transaction_dt String, transaction_amt Float32, other_id String, tran_id String, file_num String, memo_cd String, memo_text String, sub_id String') \
INTO OUTFILE 'foo.parquet' FORMAT parquet"
```